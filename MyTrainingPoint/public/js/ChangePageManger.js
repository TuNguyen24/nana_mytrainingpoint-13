//-----------------------DEFINE FOR EACH BUTTON -------------------------------------
var THAM_GIA_BUTTON   = 1;
var HOAT_DONG_BUTTON  = 2;
var GOI_Y_BUTTON      = 3;
var LO_TRINH_BUTTON   = 4;
var LIEN_HE_BUTTON    = 5;
var PROFILE_BUTTON    = 6;
var HELP_BUTTON       = 7;
var DOIMATKHAU_BUTTON = 8;
var DANGNHAP_BUTTON   = 9;
var TRANGCHU_BUTTON   = 10;
//-----------------------------END OF DEFINE------------------------------------------


//------------------------DEFINE FOR HREF OF EACH LINK--------------------------------
var THAM_GIA_HREF  = "../../Screen4_Tham gia/code_Screen4/htmlS4.html";
var HOAT_DONG_HREF = "../../Screen3_Hoat dong/code_Screen3/htmlS3.html";
var GOI_Y_HREF     = "../../Screen5_Goi y/code_Screen5/htmlS5.html";
var LO_TRINH_HREF  = "../../Screen6_Lo trinh/code_Screen6/htmlS6.html";
var LIEN_HE_HREF   = "../../Screen7_Lien he/code_Screen7/htmlS7.html";
var PROFILE_HREF   = "../../Screen9_Profile/code_Screen9/htmlS9.html";
var HELP_HREF      = "../../Screen8_Help/code_Screen8/htmlS8.html";
var DOIMATKHAU_HREF= "../../Screen11_Doi mat khau/code_Screen11/htmlS11.html";
var DANGNHAP_HREF  = "../../Screen1_Dang nhap/code_Screen1/htmlS1.html";
var TRANGCHU_HREF  = "../../Screen2_Trang chu/code_Screen2/htmlS2.html";
//------------------------------END OF DEFINE------------------------------------------

/*-------------------------------------------------------------------------------------
 * function ButtonOpenNewPageOnClick(TYPE_BUTTON) :
 * The function for when user click mouse to a links then open a new page at the same
 * tab or same windows
 *    THAM_GIA_BUTTON   1
 *    HOAT_DONG_BUTTON  2
 *    GOI_Y_BUTTON      3
 *    LO_TRINH_BUTTON   4
 *    LIEN_HE_BUTTON    5
 *    PROFILE_BUTTON    6
 *    HELP_BUTTON       7
 *    DOIMATKHAU_BUTTON 8
 *    DANGNHAP_BUTTON   9
 *    TRANGCHU_BUTTON   10
 *--------------------------------------------------------------------------------------*/
function ButtonOpenNewPageOnClick(TYPE_BUTTON) {
    var Link;
    switch(TYPE_BUTTON){
        case THAM_GIA_BUTTON: {
            Link = THAM_GIA_HREF;
            break;
        }
        case HOAT_DONG_BUTTON:{
            Link = HOAT_DONG_HREF;
            break;
        }
        case GOI_Y_BUTTON:{
            Link = GOI_Y_HREF;
            break;
        }
        case LO_TRINH_BUTTON:{
            Link = LO_TRINH_HREF;
            break;
        }
        case LIEN_HE_BUTTON:{
            Link = LIEN_HE_HREF;
            break;
        }
        case PROFILE_BUTTON:{
            Link = PROFILE_HREF;
            break;
        }

        case HELP_BUTTON:{
            Link = HELP_HREF;
            break;
        }

        case DOIMATKHAU_BUTTON:{
            Link = DOIMATKHAU_HREF;
            break;
        }

        case DANGNHAP_BUTTON:{
            Link = DANGNHAP_HREF;
            break;
        }

        case TRANGCHU_BUTTON:{
            Link = TRANGCHU_HREF;
            break;
        }

        default:{
            break;
        }
    }
    location.replace(Link);
}
//-------------------------------------END OF FUNCTION-----------------------------------